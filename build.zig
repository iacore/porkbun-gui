const std = @import("std");
const ZT = @import("lib/ZT/build.zig");
const zelda = @import("lib/zelda/build.zig");
const zasync = @import("lib/zasync/build.zig");

const FontCandidate = struct {
    path: [:0]const u8,
    size: f32,
};

const font_search_list = [_]FontCandidate{
    .{ .path = "/usr/share/fonts/noto/NotoSans-Regular.ttf", .size = 18.0 },
    .{ .path = "/usr/share/fonts/liberation/LiberationMono-Regular.ttf", .size = 18.0 },
};

fn find_default_font() !FontCandidate {
    for (font_search_list) |candidate| {
        const file = std.fs.openFileAbsolute(candidate.path, .{}) catch continue;
        file.close();
        return candidate;
    }
    // if you see this error, edit 'font_search_list' above to use your font
    // The ./find-fonts in this repo will help you find suitable fonts
    return error.NoDefaultFontFound;
}

pub fn build(b: *std.build.Builder) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    // compile-time flags
    const options = b.addOptions();
    const do_mock = b.option(bool, "mock", "Show mock windows (for testing) on startup") orelse false;
    options.addOption(bool, "MOCK", do_mock);
    //{
    //    const default_font = try find_default_font();
    //    options.addOption([:0]const u8, "default_font_path", default_font.path);
    //    options.addOption(f32, "default_font_size", default_font.size);
    //}
    options.addOption(f32, "default_font_size", 18.0);


    const exe = b.addExecutable("porkbun-gui", "src/main.zig");
    exe.main_pkg_path = ".";
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();

    ZT.link(exe);
    exe.addPackage(options.getPackage("_build_options"));
    const use_system_libressl = b.option(bool, "use-system-libressl", "Link and build from the system installed copy of LibreSSL instead of building it from source") orelse true;
    if (!use_system_libressl) {
        @panic("Building libressl from source with zelda is not supported yet. Remove this line to see if it works for you");
    }
    if (use_system_libressl) {
        exe.addIncludePath("/usr/include/libressl");
        exe.addLibraryPath("/usr/lib/libressl");
    }
    try zelda.link(b, exe, target, mode, use_system_libressl);
    exe.addPackage(zasync.getPkg());

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const exe_tests = b.addTest("src/main.zig");
    exe_tests.setTarget(target);
    exe_tests.setBuildMode(mode);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&exe_tests.step);
}
