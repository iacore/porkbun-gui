This software uses Porkbun's HTTP API to help you manage your DNS records on Porkbun.

## Environment Variables

Set those prior to application launch.

**PORKBUN_PUBLIC**: API Key. Get yours at https://porkbun.com/account/api
**PORKBUN_PRIVATE**: API Secret
**PORKBUN_DOMAIN**: Default domain name, so you don't have to type the domain name everytime you use this application
