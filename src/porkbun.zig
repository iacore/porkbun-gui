/// Porkbun API
const std = @import("std");
const zelda = @import("zelda");

const String = []const u8;
const Allocator = std.mem.Allocator;

pub const Credentials = struct {
    secretapikey: String,
    apikey: String,

    pub const empty: @This() = .{ .secretapikey = "", .apikey = "" };
};

pub fn Response(comptime T: type) type {
    return struct {
        pub const DataType: type = T;
        
        data: T,
        allocator: Allocator,

        fn parse(allocator: Allocator, input: []const u8) !@This() {
            var tokens = std.json.TokenStream.init(input);
            return .{
                .data = try std.json.parse(T, &tokens, .{ .allocator = allocator, .ignore_unknown_fields = true }),
                .allocator = allocator,
            };
        }
        pub fn deinit(self: @This()) void {
            std.json.parseFree(T, self.data, .{ .allocator = self.allocator });
        }
    };
}

test "Response.parse - parse JSON response" {
    const RESPONSE = @embedFile("test/response-getall.json");
    const response = try Response(Result_FetchAll).parse(std.testing.allocator, RESPONSE);
    defer response.deinit();
    const data = response.data;
    try std.testing.expectEqualStrings(data.status, "SUCCESS");
}

pub const DnsRecord = struct {
    id: u32 = 0,
    name: ?String = "",
    type: String = "",
    content: String = "",
    ttl: i32 = 600,
    prio: ?i32 = 0,
    notes: ?String = "",

    pub fn valid(self: @This()) bool {
        return self.ttl > 0 and self.type.len != 0;
    }
};

pub const nameToResult = struct {
    pub const fetchAll = Result_FetchAll;
    pub const create = Result_Create;
    pub const update = Result_Update;
    pub const delete = Result_Delete;
};

const Result_Mixin = struct {
    pub fn isSuccess(this: anytype) bool {
        return std.mem.eql(u8, this.status, "SUCCESS");
    }
};

pub const Result_FetchAll = struct {
    status: String,
    records: []DnsRecord,
    pub usingnamespace Result_Mixin;
};

pub const Result_Create = struct {
    status: String,
    id: String,
    pub usingnamespace Result_Mixin;
};

pub const Result_Update = struct {
    status: String,
    pub usingnamespace Result_Mixin;
};

pub const Result_Delete = struct {
    status: String,
    pub usingnamespace Result_Mixin;
};

pub const Client = struct {
    allocator: Allocator,
    client: *zelda.HttpClient,

    pub fn init(alloc: Allocator) !@This() {
        return .{
            .allocator = alloc,
            .client = try zelda.HttpClient.init(alloc, .{}),
        };
    }

    pub fn deinit(self: @This()) void {
        defer self.client.deinit();
    }

    pub fn sendRequest(self: *@This(), url: String, payload: anytype, comptime RetT: type) !Response(RetT) {
        const body = try std.json.stringifyAlloc(self.allocator, payload, .{});
        defer self.allocator.free(body);

        var request = zelda.request.Request{
            .method = .POST,
            .url = url,
            .body = zelda.request.Body{
                .kind = .JSON,
                .bytes = body,
            },
            .use_global_connection_pool = true,
        };

        std.log.info("> {s} {s}", .{ url, body });
        var response = try self.client.perform(request);
        defer response.deinit();
        if (response.status_code != @intToEnum(zelda.hzzp.StatusCode, 200)) {
            return error.RequestFailed;
        }
        if (response.body) |body2| {
            std.log.info("< {} {s}", .{ response.status_code, body2 });
            return try Response(RetT).parse(self.allocator, body2);
        } else {
            std.log.info("< {} (Response no body)", .{response.status_code});
            return error.RequestFailed;
        }
    }

    /// https://porkbun.com/api/json/v3/dns/create/DOMAIN
    pub fn fetchAll(self: *@This(), creds: Credentials, domain_name: []const u8) !Response(Result_FetchAll) {
        const url = try std.fmt.allocPrint(self.allocator, "https://porkbun.com/api/json/v3/dns/retrieve/{s}", .{domain_name});
        defer self.allocator.free(url);
        const payload = .{
            .secretapikey = creds.secretapikey,
            .apikey = creds.apikey,
        };
        return self.sendRequest(url, payload, @field(nameToResult, @src().fn_name));
    }

    pub fn create(self: *@This(), creds: Credentials, domain_name: []const u8, record: DnsRecord) !Response(Result_Create) {
        const url = try std.fmt.allocPrint(self.allocator, "https://porkbun.com/api/json/v3/dns/create/{s}", .{domain_name});
        defer self.allocator.free(url);
        const payload = .{
            .secretapikey = creds.secretapikey,
            .apikey = creds.apikey,
            .name = record.name,
            .type = record.type,
            .content = record.content,
            .ttl = record.ttl,
            .prio = record.prio,
        };
        return self.sendRequest(url, payload, @field(nameToResult, @src().fn_name));
    }

    pub fn update(self: *@This(), creds: Credentials, domain_name: []const u8, record: DnsRecord) !Response(Result_Update) {
        const url = try std.fmt.allocPrint(self.allocator, "https://porkbun.com/api/json/v3/dns/edit/{s}/{}", .{domain_name, record.id});
        defer self.allocator.free(url);
        const payload = .{
            .secretapikey = creds.secretapikey,
            .apikey = creds.apikey,
            .name = record.name,
            .type = record.type,
            .content = record.content,
            .ttl = record.ttl,
            .prio = record.prio,
        };
        return self.sendRequest(url, payload, @field(nameToResult, @src().fn_name));
    }

    pub fn delete(self: *@This(), creds: Credentials, domain_name: []const u8, id: u32) !Response(Result_Delete) {
        const url = try std.fmt.allocPrint(self.allocator, "https://porkbun.com/api/json/v3/dns/delete/{s}/{}", .{domain_name, id});
        defer self.allocator.free(url);
        const payload = .{
            .secretapikey = creds.secretapikey,
            .apikey = creds.apikey,
        };
        return self.sendRequest(url, payload, @field(nameToResult, @src().fn_name));
    }
};

pub fn domainNameValid(name: []const u8) bool {
    return name.len != 0;
}
