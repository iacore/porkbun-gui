const std = @import("std");

const Allocator = std.mem.Allocator;

pub fn LinkedListNode(comptime T: type) type {
    return struct {
        prev: ?*@This(),
        next: ?*@This(),
        data: T,
    };
}

pub fn LinkedList(comptime T: type) type {
    return struct {
        allocator: Allocator,
        head: ?*Node = null,
        tail: ?*Node = null,

        const Node = LinkedListNode(T);
        const Self = @This();

        pub fn init(a: Allocator) Self {
            return .{
                .allocator = a,
            };
        }

        pub fn deinit(self: Self) void {
            var maybe_curr = self.head;
            while (maybe_curr) |curr| {
                maybe_curr = curr.next;
                self.remove_nocheck(curr);
            }
        }

        pub fn append(self: *Self, data: T) !void {
            var node = try self.allocator.create(Node);
            node.* = .{
                .prev = null,
                .next = null,
                .data = data,
            };
            if (self.tail) |tail| {
                tail.next = node;
                node.prev = tail;
            } else {
                self.head = node;
            }
            self.tail = node;
        }

        pub fn remove(self: *Self, node: *Node) void {
            if (node.prev) |prev| {
                prev.next = node.next;
            } else {
                // this note is the first
                self.head = node.next;
            }
            if (node.next) |next| {
                next.prev = node.prev;
            } else {
                // this note is the last
                self.tail = node.prev;
            }
            self.remove_nocheck(node);
        }

        fn remove_nocheck(self: Self, node: *Node) void {
            if (@hasField(@TypeOf(node.data), "deinit")) {
                node.data.deinit();
            }
            self.allocator.destroy(node);
        }
    };
}
