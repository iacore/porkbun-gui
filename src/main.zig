const std = @import("std");
const zt = @import("zt");
const ig = @import("imgui");
const zelda = @import("zelda");
const build_options = @import("_build_options");
const porkbun = @import("porkbun.zig");
const window = @import("window.zig");

test {
    _ = porkbun;
    _ = window;
}

const Allocator = std.mem.Allocator;

const event_loop: std.event.Loop = .{};

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{ .safety = true }){};
    const allocator = gpa.allocator();
    defer {
        _ = gpa.deinit(); // detect leaks
    }

    var creds = porkbun.Credentials.empty;
    if (std.os.getenvZ("PORKBUN_PUBLIC")) |apikey| {
        creds.apikey = apikey;
    }
    if (std.os.getenvZ("PORKBUN_SECRET")) |secretapikey| {
        creds.secretapikey = secretapikey;
    }
    const default_domain_name = if (std.os.getenvZ("PORKBUN_DOMAIN")) |domain_name|
        domain_name
    else
        "";

    var context = try zt.App(window.Root).beginWithData(allocator, try window.Root.init(allocator, creds, default_domain_name));
    defer context.deinit();
    defer context.data.deinit();
    defer zelda.client.global_connection_cache.deinit();

    // Lets customize!
    const io = ig.igGetIO();
    io.*.IniFilename = "imgui.ini";
    // const font = context.addFont(build_options.default_font_path, build_options.default_font_size);
    const font = context.addFontFromMemory(@embedFile("../assets/Atkinson_Hyperlegible_Regular.10.ttf"), build_options.default_font_size);
    io.*.FontDefault = font;

    context.settings.energySaving = false;
    context.setWindowTitle("Porkbun DNS Manager");
    // context.setWindowIconFromMemory(@embedFile("../assets/ico.png"));

    if (build_options.MOCK) {
        context.data.domain_name = "example.com";
        try context.data.createNewRecordWindow();
    }

    // You control your own main loop, all you got to do is call begin and end frame,
    // and zt will handle the rest.
    while (context.open) {
        context.beginFrame();
        defer context.endFrame();
        try context.data.show();
    }
}

const stb = @import("stb_image");
const glfw = @import("glfw");

const Context = zt.App(window.Root).Context;
