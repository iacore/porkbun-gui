const std = @import("std");
const zt = @import("zt");
const ig = @import("imgui");
const zg = zt.custom_components;
const glfw = @import("glfw");
const zasync = @import("zasync");
const build_options = @import("_build_options");
const porkbun = @import("porkbun.zig");

const Allocator = std.mem.Allocator;
const LinkedList = @import("ds.zig").LinkedList;
const igDefaultWindowFlags = ig.ImGuiWindowFlags_AlwaysAutoResize | ig.ImGuiWindowFlags_NoResize | ig.ImGuiWindowFlags_NoDocking;
const STR_NA = "N/A";

pub const Root = Window_Root;

pub const IWindow = struct {
    const ShowFn = *const fn (_: *anyopaque) anyerror!bool;
    const DeinitFn = *const fn (_: *const anyopaque) void;

    _show: ShowFn,
    _deinit: DeinitFn,
    _data: [2048]u8,

    pub fn from(concrete: anytype) @This() {
        const T = @TypeOf(concrete);
        const S = @sizeOf(T);
        if (S > 2048) {
            @compileLog("Type too large to fit in here", S);
        }
        const PlugImpl = struct {
            fn deinit_plug(self: *const T) void {
                return T.deinit(self.*);
            }
        };
        var me = .{
            ._show = @ptrCast(ShowFn, &T.show),
            ._deinit = @ptrCast(DeinitFn, &PlugImpl.deinit_plug),
            ._data = std.mem.zeroes([2048]u8),
        };
        @memcpy(&me._data, @ptrCast([*]const u8, &concrete), S);
        return me;
    }

    pub fn show(self: *@This()) !bool {
        return self._show(@ptrCast(*anyopaque, &self._data));
    }
    pub fn deinit(self: *const @This()) void {
        return self._deinit(@ptrCast(*const anyopaque, &self._data));
    }
};

fn Window_WithFuture(comptime Inner: type) type {
    return struct {
        allocator: Allocator,
        ig_sid: u16, // for unique windows
        open: bool = true,
        domain_name: [:0]const u8,
        root: *Window_Root,
        future: ?*zasync.Future(Inner.ResponseType),
        inner: Inner,

        pub fn init(alloc: Allocator, sid: u16, domain_name: []const u8, root: *Window_Root, inner: Inner) !@This() {
            var me = @This(){
                .ig_sid = sid,
                .allocator = alloc,
                .domain_name = try alloc.dupeZ(u8, domain_name),
                .root = root,
                .future = null,
                .inner = inner,
            };
            try me.sendRequest();
            return me;
        }

        pub fn sendRequest(self: *@This()) !void {
            if (self.future) |future| future.deinit();
            self.future = try self.inner.sendRequest(self.allocator, &self.root.client, self.root.creds, self.domain_name);
        }

        pub fn deinit(self: @This()) void {
            if (self.future) |future| future.deinit();
            self.inner.deinit();
            self.allocator.free(self.domain_name);
        }

        pub fn show(self: *@This()) !bool {
            const window_name = zg.fmtTextForImgui(Inner.title_fmt, .{self.ig_sid});
            if (ig.igBegin(window_name.ptr, &self.open, ig.ImGuiWindowFlags_NoDocking) and self.open) {
                zg.text("Domain name: {s}", .{self.domain_name});
                ig.igSameLine(0, -1);
                if (ig.igButton("Retry Request", .{})) {
                    self.sendRequest() catch |err| {
                        std.log.err("On send request: {}", .{err});
                    };
                }

                if (self.future) |future| {
                    if (!future.done.isSet()) {
                        ig.igSameLine(0, -1);
                        zg.text("Waiting for Response", .{});
                    } else {
                        if (future.data.?) |response| {
                            try self.inner.show(self.root, self.domain_name, response);
                        } else |err| {
                            zg.text("HTTP Request failed: {}", .{err});
                        }
                    }
                } else {
                    ig.igText("Error: self.future == null");
                    ig.igText("This shouldn't happen");
                }
            }
            ig.igEnd();
            return self.open;
        }
    };
}

const Window_FetchAll = Window_WithFuture(struct {
    pub const title_fmt = "Fetch All Records #{}";

    const f = porkbun.Client.fetchAll;
    pub const ResponseType = @typeInfo(@TypeOf(f)).Fn.return_type.?;

    selected: ?porkbun.DnsRecord = null,

    pub fn sendRequest(_: *const @This(), allocator: Allocator, client: *porkbun.Client, creds: porkbun.Credentials, domain_name: []const u8) !*zasync.Future(ResponseType) {
        return try zasync.Task(f).launch(allocator, .{ client, creds, domain_name });
    }

    const COLUMNS = .{ "id", "type", "name", "content", "TTL", "priority", "notes" };

    pub fn show(self: *@This(), root: *Window_Root, domain_name: []const u8, response: porkbun.Response(porkbun.Result_FetchAll)) !void {
        const result: porkbun.Result_FetchAll = response.data;
        if (result.isSuccess()) {
            ig.igSameLine(0, -1);
            if (ig.igButton("Print to stdout", .{})) {
                const writer = std.io.getStdOut().writer();
                std.json.stringify(result, .{}, writer) catch |err| {
                    std.log.err("On Print Table: {}", .{err});
                };
            }

            try self.showTable(root, domain_name, result);
        } else {
            zg.text("Request successfully failed: {s}", .{result.status});
        }
    }

    fn showTable(self: *@This(), root: *Window_Root, domain_name: []const u8, result: porkbun.Result_FetchAll) !void {
        if (self.selected) |s| {
            zg.text("Selected Record: {}", .{s.id});
        } else {
            zg.text("Selected Record: {s}", .{STR_NA});
        }
        if (self.selected) |selected| {
            ig.igSameLine(0, -1);
            if (ig.igSmallButton("Edit")) {
                try root.createEditWindow(selected);
            }
            ig.igSameLine(0, -1);
            if (ig.igSmallButton("Delete")) {
                try root.createSimpleRequestWindow("delete", .{ domain_name, selected.id });
            }
        }

        if (ig.igBeginTable("Records", COLUMNS.len, ig.ImGuiTableFlags_Resizable, .{}, 0)) {
            inline for (COLUMNS) |column| {
                ig.igTableSetupColumn(column, 0, 0, 0);
            }
            ig.igTableHeadersRow();
            for (result.records) |record| {
                ig.igTableNextRow(0, 0);
                if (ig.igTableNextColumn()) {
                    var checked = if (self.selected) |selected| record.id == selected.id else false;
                    const s = zg.fmtTextForImgui("{}", .{record.id});
                    if (ig.igCheckbox(s.ptr, &checked)) {
                        if (checked) {
                            self.selected = record;
                        } else {
                            self.selected = null;
                        }
                    }
                }
                if (ig.igTableNextColumn()) zg.text("{s}", .{record.type});
                if (ig.igTableNextColumn()) zg.text("{s}", .{record.name orelse STR_NA});
                if (ig.igTableNextColumn()) zg.text("{s}", .{record.content});
                if (ig.igTableNextColumn()) zg.text("{}", .{record.ttl});
                if (ig.igTableNextColumn()) {
                    if (record.prio) |prio| {
                        zg.text("{}", .{prio});
                    } else {
                        ig.igText(STR_NA);
                    }
                }
                if (ig.igTableNextColumn()) zg.text("{s}", .{record.notes orelse STR_NA});
            }
        }
        ig.igEndTable();
    }

    pub fn deinit(_: @This()) void {}
});

const Window_NewOrEditRecord = struct {
    allocator: Allocator,
    buffers: [4][128]u8 = std.mem.zeroes([4][128]u8),
    ig_sid: u16, // for unique windows
    domain_name: [:0]const u8,
    params: porkbun.DnsRecord,
    open: bool = true,
    root: *Window_Root,
    mode: enum { edit, new },

    pub fn initNew(alloc: Allocator, sid: u16, domain_name: []const u8, root: *Window_Root) !@This() {
        const buf = try alloc.dupeZ(u8, domain_name);
        return .{
            .mode = .new,
            .root = root,
            .ig_sid = sid,
            .allocator = alloc,
            .domain_name = buf,
            .params = .{},
        };
    }

    pub fn initEdit(alloc: Allocator, sid: u16, domain_name: []const u8, root: *Window_Root, record: porkbun.DnsRecord) !@This() {
        const buf = try alloc.dupeZ(u8, domain_name);
        return .{
            .mode = .edit,
            .root = root,
            .ig_sid = sid,
            .allocator = alloc,
            .domain_name = buf,
            .params = record,
        };
    }

    pub fn deinit(self: @This()) void {
        self.allocator.free(self.domain_name);
    }
    pub fn deinit_ptr(self: *const @This()) void {
        self.deinit();
    }

    pub fn show(self: *@This()) !bool {
        const window_name = switch (self.mode) {
            .new => zg.fmtTextForImgui("New Record #{}", .{self.ig_sid}),
            .edit => zg.fmtTextForImgui("Update Record {} #{}", .{ self.params.id, self.ig_sid }),
        };
        if (ig.igBegin(window_name.ptr, &self.open, igDefaultWindowFlags) and self.open) {
            zg.text("Domain name: {s}", .{self.domain_name});
            if (self.params.name == null) self.params.name = "";
            _ = igInputText("name", &self.buffers[0], 0, &self.params.name.?);
            _ = igInputText("type", &self.buffers[1], 0, &self.params.type);
            _ = igInputText("content", &self.buffers[2], 0, &self.params.content);
            _ = ig.igInputInt("TTL", &self.params.ttl, 5, 10, 0);
            _ = ig.igInputInt("priority", &(self.params.prio.?), 1, 3, 0);
            _ = igInputText("notes", &self.buffers[3], 0, &(self.params.notes.?));
            if (ig.igButton("Submit", .{})) {
                switch (self.mode) {
                    .new => {
                        try self.root.createSimpleRequestWindow("create", .{ self.domain_name, self.params });
                    },
                    .edit => {
                        try self.root.createSimpleRequestWindow("update", .{ self.domain_name, self.params });
                    },
                }
            }
        }
        ig.igEnd();
        return self.open;
    }
};

const Window_Root = struct {
    allocator: std.mem.Allocator,

    ig_show_password: bool = false,
    ig_windows: LinkedList(IWindow),
    ig_enable_domain_actions: bool = false,
    ig_next_sid: u16 = 0,

    buffers: [3][128]u8,
    creds: porkbun.Credentials,
    domain_name: []const u8 = "",

    client: porkbun.Client,

    pub fn init(allocator: Allocator, creds: porkbun.Credentials, default_domain_name: []const u8) !@This() {
        var me = @This(){
            .allocator = allocator,
            .ig_windows = LinkedList(IWindow).init(allocator),
            .client = try porkbun.Client.init(allocator),
            .buffers = std.mem.zeroes([3][128]u8),
            .creds = creds,
            .domain_name = default_domain_name,
        };
        me.validateDomainName();
        std.mem.copy(u8, &me.buffers[0], creds.apikey);
        std.mem.copy(u8, &me.buffers[1], creds.secretapikey);
        std.mem.copy(u8, &me.buffers[2], default_domain_name);
        return me;
    }

    pub fn deinit(self: @This()) void {
        self.client.deinit();
        self.ig_windows.deinit();
    }

    pub fn validateDomainName(self: *@This()) void {
        self.ig_enable_domain_actions = porkbun.domainNameValid(self.domain_name);
    }

    pub fn show(self: *@This()) !void {
        if (ig.igBegin("Root Window", null, igDefaultWindowFlags)) {
            if (ig.igCollapsingHeader_TreeNodeFlags("About & Usage", 0)) {
                ig.igPushTextWrapPos(0.0);
                zg.text("{s}", .{@embedFile("about.md")});
                ig.igPopTextWrapPos();
            }

            if (ig.igCollapsingHeader_TreeNodeFlags("Credentials", 0)) {
                const text_flags = if (self.ig_show_password) 0 else ig.ImGuiInputTextFlags_Password;
                _ = igInputText("API Key", &self.buffers[0], text_flags, &self.creds.apikey);
                _ = igInputText("API Secret", &self.buffers[1], text_flags, &self.creds.secretapikey);
                _ = ig.igCheckbox("Show Credentials", &self.ig_show_password);
            }

            ig.igSeparator();
            ig.igText("Actions");
            if (igInputText("Domain Name", &self.buffers[2], 0, &self.domain_name)) {
                self.validateDomainName();
            }

            ig.igPushItemFlag(ig.ImGuiItemFlags_Disabled, true);
            _ = ig.igCheckbox("Valid?", &self.ig_enable_domain_actions);
            ig.igPopItemFlag();

            ig.igSameLine(0, -1);
            ig.igPushItemFlag(ig.ImGuiItemFlags_Disabled, !self.ig_enable_domain_actions);
            if (ig.igButton("Fetch Records", .{})) {
                try self.createRequestWindow();
            }
            ig.igSameLine(0, -1);
            if (ig.igButton("New Record", .{})) {
                try self.createNewRecordWindow();
            }
            ig.igPopItemFlag();
        }
        ig.igEnd();

        try self.showAllNewRecordWindow();
    }

    pub fn showAllNewRecordWindow(self: *@This()) !void {
        // const MaybeNodePtr = @TypeOf(self.ig_windows.head);
        var maybe_curr = self.ig_windows.head;
        while (maybe_curr) |curr| {
            maybe_curr = curr.next;
            if (!try curr.data.show()) {
                self.ig_windows.remove(curr);
            }
        }
    }

    fn createWindow(self: *@This(), window: anytype) !void {
        self.ig_next_sid += 1;
        try self.ig_windows.append(IWindow.from(window));
    }

    pub fn createRequestWindow(self: *@This()) !void {
        const window = try Window_FetchAll.init(self.allocator, self.ig_next_sid, self.domain_name, self, .{});
        return self.createWindow(window);
    }

    pub fn createNewRecordWindow(self: *@This()) !void {
        const window = try Window_NewOrEditRecord.initNew(self.allocator, self.ig_next_sid, self.domain_name, self);
        return self.createWindow(window);
    }

    pub fn createEditWindow(self: *@This(), record: porkbun.DnsRecord) !void {
        const window = try Window_NewOrEditRecord.initEdit(self.allocator, self.ig_next_sid, self.domain_name, self, record);
        return self.createWindow(window);
    }

    pub fn createSimpleRequestWindow(self: *@This(), comptime fname: []const u8, args: anytype) !void {
        const Task = zasync.Task(@field(porkbun.Client, fname));
        const Impl = struct {
            pub const title_fmt = "Client." ++ fname ++ "() #{}";

            pub const ResponseType = Task.ReturnType;
            const RetT = @field(porkbun.nameToResult, fname);

            args: @TypeOf(args),

            pub fn sendRequest(self2: *const @This(), allocator: Allocator, client: *porkbun.Client, creds: porkbun.Credentials, domain_name: []const u8) !*zasync.Future(ResponseType) {
                _ = domain_name;
                return try Task.launch(allocator, .{ client, creds } ++ self2.args);
            }
            pub fn show(_: *@This(), root: *Window_Root, domain_name: []const u8, response: porkbun.Response(RetT)) !void {
                _ = .{ root, domain_name };
                const data: RetT = response.data;
                const s = try std.json.stringifyAlloc(zt.Allocators.ring(), data, .{});
                zg.text("{s}", .{s});
            }

            fn deinit(_: @This()) void {}
        };
        const window = try Window_WithFuture(Impl).init(self.allocator, self.ig_next_sid, "", self, .{
            .args = args,
        });
        return self.createWindow(window);
    }
};

// ===== utils =====

pub fn igInputText(label: []const u8, buf: []u8, text_flags: ig.ImGuiTextFlags, cache: *[]const u8) bool {
    const changed = ig.igInputText(label.ptr, buf.ptr, buf.len, text_flags, null, null);
    if (changed) {
        cache.* = std.mem.sliceTo(buf, 0);
    }
    return changed;
}
